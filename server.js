/**
 * Node.js Base v2
 */
var express = require('express'),
	colors = require('colors'),
	url = require("url"),
	fs = require('fs'),
	http = require("http"),
	ejs = require('ejs');

// Create Server
var server = express.createServer();

// Configure
server.configure(function(){
	server.set('title', '');
	server.set('views', __dirname + '/views');
	server.set('view engine', 'ejs');
	server.use(express.bodyParser());
  	server.use(express.methodOverride());
	server.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
  	server.use(express.cookieParser());
  	server.use(express.session({ secret: 'some random phrase here' }));
  	server.use(server.router);
  	server.use(express.static(__dirname + '/public'));
  	server.set('view options', { layout: true });	
});

// Route Sepeartion
var main_route = require('./routes/main');

// Routing
server.get('/', main_route.index);

// Port
var port = process.env.PORT || 3000;
server.listen(port, function(){
	var addr = server.address();
	console.log('Application running and is available at http://' + addr.address + ':' + addr.port);
});
